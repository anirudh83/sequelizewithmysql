<h1>Node.js Rest APIs example with Express, Sequelize & MySQL</h1>
<h3>For Testing Movie api</h3>
</br></br>
<h3>GET Request</h3>

1. https://api-movie-anirudh.herokuapp.com/api/movies </br>
2. https://api-movie-anirudh.herokuapp.com/api/genre </br>
3. https://api-movie-anirudh.herokuapp.com/api/director </br>

</br></br>
<h3>POST Request</h3>

1. https://api-movie-anirudh.herokuapp.com/api/director </br>
2. https://api-movie-anirudh.herokuapp.com/api/genre </br>
3. https://api-movie-anirudh.herokuapp.com/api/movies </br>
        <h4>json data</h4>
        {
        "Rank": 51,
        "Title": "Casablanca II",
        "Description": "A cynical nightclub owner protects an old flame and her husband from Nazis in Morocco.",
        "Runtime": 102,
        "Genre": "Drama and Action",
        "Rating": 8.5,
        "Metascore": 100,
        "Votes": 441864,
        "Gross_Earning_in_Mil": 1.02,
        "Director": "Michael Curtiz",
        "Actor": "Humphrey Bogart",
        "Year": 1942
        }


</br></br>
<h3>UPDATE Request</h3>

1. https://api-movie-anirudh.herokuapp.com/api/director/1</br>
2. https://api-movie-anirudh.herokuapp.com/api/genre/1</br>
3. https://api-movie-anirudh.herokuapp.com/api/movies/1</br>
  <h4>json data</h4>
        {
        "Rank": 51,
        "Title": "Casablanca II",
        "Description": "A cynical nightclub owner protects an old flame and her husband from Nazis in Morocco.",
        "Runtime": 102,
        "Genre": "Drama and Action",
        "Rating": 8.5,
        "Metascore": 100,
        "Votes": 441864,
        "Gross_Earning_in_Mil": 1.02,
        "Director": "Michael Curtiz",
        "Actor": "Humphrey Bogart",
        "Year": 1942
        }


</br></br>
<h3>DELETE Request</h3>

1. https://api-movie-anirudh.herokuapp.com/api/director/1</br>
2. https://api-movie-anirudh.herokuapp.com/api/genre/1</br>
3. https://api-movie-anirudh.herokuapp.com/api/movies/1</br>



<h3>Create User</h3>
<h4>json data</h4>
{
    "user":"anirudh",
    "password": "anirudh"
}


POST: https://api-movie-anirudh.herokuapp.com/api/user

<h3>Get Token</h3>
<h4>json data</h4>
{
    "user":"anirudh",
    "password": "anirudh"
}

POST:  https://api-movie-anirudh.herokuapp.com/api/user/login

 