const db = require("../models");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Genres = db.genres;
const Director = db.directors;
const Movies = db.movies;
const Op = db.Sequelize.Op;
require('dotenv').config();

function validateData(data) {
        if (data === undefined || data == "NA" ){
                return null;
        }else{
                return data;
        }
}

exports.director = (req, res) => {

        let movies = req.body;
        if(Array.isArray(req.body) == true){
                uniqueDirector = [];
                movies.forEach(movie=>{
                        let directorName = validateData(movie['Director']);
                        if(uniqueDirector.includes(directorName) == false && directorName != null){
                                uniqueDirector.push(directorName);
                        }
                })
                uniqueDirector.forEach((name) =>{
                        director_object={
                                director_name :name
                        };
                        Director
                        .create(director_object)
                        .catch(err => {
                                res.status(500).json({
                                message: "internal server error"
                                });
                                res.end();
                        });
                })
                res.status(200).send(req.body).end();
       }else{
                res.status(400).send({
                        message: "Invalid Input"
                }).end();
       }
      
}

exports.genres = (req, res)=>{
        let movies = req.body;
        if(Array.isArray(req.body) == true){
                 uniqueGenres = [];
                 movies.forEach(movie=>{
                         genreType =validateData(movie['Genre'])
                         if(uniqueGenres.includes(genreType) == false && genreType != null){
                                uniqueGenres.push(genreType);
                         }
                 })
                 uniqueGenres.forEach( (type) =>{
                        genre_object={
                        genre : type
                        };

                         Genres
                        .create(genre_object)
                        .catch(err => {
                                res.status(500).json({
                                message: "internal server error"
                                });
                                res.end();
                        });
                 })
                 res.status(200).send(req.body).end();
        }
}

exports.movies = (req, res) => {
        let movies = req.body;
        if(Array.isArray(req.body) == true){ 
                
                movies.forEach( (movie) =>{
                        
                        movies_object={
                                rank : validateData(movie['Rank']),
                                title : validateData(movie['Title']),
                                description : validateData(movie['Description']),
                                runtime : validateData(movie['Runtime']),
                                rating : validateData(movie['Rating']),
                                metascore : validateData(movie['Metascore']),
                                votes : validateData(movie['Votes']),
                                gross_earning_in_mil : validateData(movie['Gross_Earning_in_Mil']),
                                actor : validateData(movie['Actor'])
                        };
                        console.log(movies_object)
                        
                        Movies.create(movies_object)
                        .then(data => {
                                res.status(201);
                        })
                        .catch(err => {
                                res.status(500).json({
                                message: "internal server error"
                                });
                                
                        });
                 })
                 res.status(200).json(req.body).end();
        }
}