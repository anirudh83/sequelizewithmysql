module.exports = (sequelize, Sequelize) => {
        const Users = sequelize.define("users", {
                user: {
                        type: Sequelize.STRING,
                        primaryKey: true
                },
                password: {
                        type: Sequelize.STRING,
                }
        });
      
        return Users;
};