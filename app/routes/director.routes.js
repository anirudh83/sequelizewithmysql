const jwt = require('jsonwebtoken');

module.exports = app => {
        const directors = require("../controllers/director.controller.js");
      
        var router = require("express").Router();
        router.use(function authenticateToken(req, res, next) {
                const authHeader = req.headers['authorization']
                const token = authHeader && authHeader.split(' ')[1]
                if (token == null){
                        return res.sendStatus(401)
                }
                         
                jwt.verify(token, process.env.ACCESS_TOKEN_SECRET ,(err, user) => {
                        console.log(err)
                        if (err) {
                                console.log(err);
                                res.status(401).json({
                                        "msg" : "authenticate fail"
                                }).end();
                        }else{
                                next();  
                        }
                        
                        
                })
        })
      
        router.post("/", directors.create);
      
        router.get("/", directors.findAll);

        router.put("/:id", directors.update);
          
        router.delete("/:id", directors.delete);
     
        app.use('/api/director', router);
};