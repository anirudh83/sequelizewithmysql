const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const jwt = require('jsonwebtoken');
const db = require("./app/models");
db.sequelize.sync();

const app = express();

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));



app.get("/", (req, res) => {
  res.json({ message: "Welcome to movies API application." });
});

require("./app/routes/director.routes")(app);
require("./app/routes/movie.routes")(app);
require("./app/routes/genre.routes")(app);
require("./app/routes/user.routes")(app);
require("./app/routes/insertData.routes")(app);


const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});